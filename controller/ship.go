package controller

import (
	"errors"
	"gitlab.com/2heoh/go-battleship/console"
	"gitlab.com/2heoh/go-battleship/letter"
	"strconv"
	"strings"
)

type Position struct {
	Column letter.Letter
	Row    int
}

type Ship struct {
	isPlaced  bool
	Name      string
	Size      int
	positions []Position
	Color     console.Color
}

func NewPosition(letter letter.Letter, number int) *Position {
	return &Position{Column: letter, Row: number}
}

func (s *Ship) AddPosition(input string) {
	if s.positions == nil {
		s.positions = []Position{}
	}

	letter := letter.FromString(string(strings.ToUpper(input)[0]))
	number, err := strconv.Atoi(string(input[1]))

	if err != nil {
		panic(err)
	}

	s.positions = append(s.positions, Position{Column: letter, Row: number})
}

/*
	  A B C D
	1
	2
	3
	4
*/
func (s *Ship) SetPosition(input string) error {
	s.positions = make([]Position, s.Size)

	l := letter.FromString(string(strings.ToUpper(input)[0]))
	number, err := strconv.Atoi(string(input[1]))
	direction := string(strings.ToUpper(input)[2])

	if err != nil {
		panic(err)
	}

	s.positions[0] = Position{
		Column: l,
		Row:    number,
	}
	switch direction {
	case "R":
		for i := 1; i < s.Size; i++ {
			newLetter := l + letter.Letter(i)

			if newLetter > letter.H {
				return errors.New("overflow right")
			}

			s.positions[i] = Position{
				Column: newLetter,
				Row:    number,
			}
		}

	case "L":
		for i := 1; i < s.Size; i++ {
			newLetter := l - letter.Letter(i)

			if newLetter < letter.A {
				return errors.New("overflow left")
			}

			s.positions[i] = Position{
				Column: newLetter,
				Row:    number,
			}
		}


	case "U":
		for i := 1; i < s.Size; i++ {
			newNumber := number - i

			if newNumber < 1 {
				return errors.New("overflow up")
			}

			s.positions[i] = Position{
				Column: l,
				Row:    newNumber,
			}
		}

	case "D":
		for i := 1; i < s.Size; i++ {
			newNumber := number + i

			if newNumber > 8 {
				return errors.New("overflow down")
			}

			s.positions[i] = Position{
				Column: l,
				Row:    newNumber,
			}
		}
	}

	return nil
}

func (s *Ship) GetPositions() []Position {
	return s.positions
}

func (s *Ship) SetPositions(p *Position) {
	s.positions = append(s.positions, *p)
}
