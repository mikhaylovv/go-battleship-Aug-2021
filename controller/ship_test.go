package controller

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/2heoh/go-battleship/console"
	"testing"
)

/*
	  A B C D
	1
	2
	3
	4
*/

func TestShipSetPosition(t *testing.T) {
	ship := NewShip("govno", 4, console.RED)
	assert.NoError(t, ship.SetPosition("a1r"))
	assert.Equal(t, "overflow left", ship.SetPosition("a1l").Error())
	assert.NoError(t, ship.SetPosition("a1d"))
	assert.Equal(t, "overflow up", ship.SetPosition("a1u").Error())
}
