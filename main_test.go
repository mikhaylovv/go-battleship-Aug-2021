package main

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/2heoh/go-battleship/controller"
	"gitlab.com/2heoh/go-battleship/letter"
	"strconv"
	"testing"
)

func TestParsePosition(t *testing.T) {
	actual := parsePosition("A1")
	expected := &controller.Position{Column: letter.A, Row: 1}

	assert.Equal(t, expected, actual)
}

func TestParsePosition2(t *testing.T) {
	//given
	var expected *controller.Position = controller.NewPosition(letter.B, 1)
	//when
	var actual *controller.Position = parsePosition("B1")
	//then
	assert.Equal(t, expected, actual)
}

func TestInitEnemyFleet(t *testing.T) {
	for i := 0; i < 5; i++ {
		initializeEnemyFleet(i)

		cells := make(map[string]bool)
		for _, ship := range enemyFleet {
			for _, position := range ship.GetPositions() {
				pStr := position.Column.String() + strconv.Itoa(position.Row)
				if _, ok := cells[pStr]; ok {
					t.Fatalf("cell positioned twice. case: %d, ship: %s, cell: %s", i, ship.Name, pStr)
				}

				cells[pStr] = true
			}
		}
	}
}